package com.example.zoholike;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextClock;
import android.widget.TextView;
import android.widget.Toast;


import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import static com.example.zoholike.R.drawable.ic_person;

public class Home extends AppCompatActivity {

    DatabaseReference reff;
    FirebaseAuth firebaseAuth;
    FirebaseUser firebaseUser;

    private DrawerLayout drawerLayout;
    private TextView name, email;
    private ImageView userImage;

    private static final int REQUEST_LOCATION = 1;
    LocationManager locationManager;
    String latitude, longitude, personName, personEmail, userID, currentDate;

    Attendence attendence;
    TextClock checkIn,checkOut;

    ProgressDialog progressDialog;

    MaterialCardView materialCardView,materialCardView2;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);


        Toolbar toolbar = findViewById(R.id.toolbar);
        NavigationView navigationView = findViewById(R.id.navigationView);
        drawerLayout = findViewById(R.id.drawer);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Home");
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.open, R.string.close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();


        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        reff = FirebaseDatabase.getInstance().getReference();
        userID = firebaseUser.getUid();



        materialCardView=findViewById(R.id.materialCardView);
        materialCardView2=findViewById(R.id.materialCardView2);
        checkIn = findViewById(R.id.textClock1);
        checkOut = findViewById(R.id.textClock2);

        final TextView userName = findViewById(R.id.user_name);
        TextView date = findViewById(R.id.date);

        progressDialog = new ProgressDialog(Home.this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage("Loading...please wait");
        progressDialog.setCancelable(true);
        progressDialog.show();


        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy:MM:dd");
        currentDate = sdf.format(new Date());
        date.setText(currentDate);

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                drawerLayout.closeDrawer(GravityCompat.START);
                switch (item.getItemId()) {
                    case R.id.history:
                        Intent i = new Intent(Home.this, AttendenceSheet.class);
                        i.putExtra("USER_ID", userID);
                        startActivity(i);
                        //Toast.makeText(Home.this, "History", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.edit:
                        Intent i1 = new Intent(Home.this, SignUp.class);
                        i1.putExtra("ACTIVITY", "2");
                        startActivity(i1);
                        //Toast.makeText(Home.this, "Signup", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.logout:
                        FirebaseAuth.getInstance().signOut();
                        finish();
                        Intent i2 = new Intent(Home.this, MainActivity.class);
                        startActivity(i2);
                        //Toast.makeText(Home.this, "Logout", Toast.LENGTH_SHORT).show();
                        break;
                }
                return true;
            }
        });

        View hView = navigationView.getHeaderView(0);
        name = hView.findViewById(R.id.user_name);
        email = hView.findViewById(R.id.email);
        userImage=hView.findViewById(R.id.person_image);

        reff.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.child("Employee").child(userID).child("name").getValue().toString().matches(" ") && dataSnapshot.child("Employee").child(userID).child("email").getValue().toString().matches(" ")) {
                    name.setText("Please fill your profile");
                } else {
                    progressDialog.dismiss();
                    personName = dataSnapshot.child("Employee").child(userID).child("name").getValue().toString();
                    personEmail = dataSnapshot.child("Employee").child(userID).child("email").getValue().toString();
                    if(dataSnapshot.child("Employee").child(userID).child("imageUrl").getValue().toString().matches(" ")) {
                    }
                    else {
                        String imageLink = dataSnapshot.child("Employee").child(userID).child("imageUrl").getValue().toString();
                        Glide.with(Home.this).load(imageLink).placeholder(R.drawable.ic_person).into(userImage);
                    }

                    name.setText(personName);
                    email.setText(personEmail);
                    userName.setText(personName);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                progressDialog.dismiss();
                Toast.makeText(Home.this, databaseError.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });



        checkIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    OnGPS();
                } else {
                    getLocation(0);
                }
            }
        });

        checkOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    OnGPS();
                } else {
                    getLocation(1);
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.finish();
            super.onBackPressed();
        }
    }

    private void OnGPS() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Enable GPS").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void getLocation(int state) {

        if (ActivityCompat.checkSelfPermission(
                Home.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                Home.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        } else {
            Location locationGPS = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if (locationGPS != null) {

                double lat = locationGPS.getLatitude();
                double longi = locationGPS.getLongitude();
                latitude = String.valueOf(lat);
                longitude = String.valueOf(longi);
                getAddress(lat, longi);

                String la= String.valueOf(lat);
                String lo=String.valueOf(longi);
                distance(33.71523833333333, 73.0853566666667, lat, longi, state);
                //Toast.makeText(this, "lat: "+la + "\n"+"long: "+ lo , Toast.LENGTH_SHORT).show();
                //showLocation.setText("Your Location: " + "\n" + "Latitude: " + latitude + "\n" + "Longitude: " + longitude);
            } else {
                Toast.makeText(this, "Unable to find location.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void getAddress(double lat, double lng) {
        Geocoder geocoder = new Geocoder(Home.this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
            Address obj = addresses.get(0);
            String add = obj.getAddressLine(0);

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void distance(double lat1, double long1, double lat2, double long2, int state) {


        //longitude difference
        double longDiff = long1 - long2;

        //Calculat distance
        double distance = Math.sin(deg2rad(lat1))
                * Math.sin(deg2rad(lat2))
                + Math.cos(deg2rad(lat1))
                * Math.cos(deg2rad(lat2))
                * Math.cos(deg2rad(longDiff));

        distance = Math.acos(distance);

        //Convert radian to degree
        distance = rad2deg(distance);

        //Distance in miles
        distance = distance * 60 * 1.1515;

        //Distance in KM and m
        double distance1 = distance * 1.6 * 1000;

        if (distance1 < 100) {
            if (state == 0) {
                ChecckIn();
            } else {
                CheckOut();
            }
        } else {
            String d = String.valueOf(distance1);
            Toast.makeText(this, d, Toast.LENGTH_SHORT).show();
            Toast.makeText(this, "Out of range", Toast.LENGTH_SHORT).show();
        }
    }

    //Convert radian to degree
    private double rad2deg(double distance) {

        return (distance * 180.0 / Math.PI);
    }

    //Convert degree to radius
    private double deg2rad(double lat) {

        return (lat * Math.PI / 180.0);
    }

    private void ChecckIn() {

        Calendar calendar = Calendar.getInstance();
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat mdformat = new SimpleDateFormat("HH:mm:ss");
        String time = mdformat.format(calendar.getTime());
        attendence = new Attendence(currentDate, time, " ");

        FirebaseDatabase.getInstance().getReference("Attendence").child(userID).child(currentDate)
                .setValue(attendence).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Toast.makeText(Home.this, "Check-in Succesfully", Toast.LENGTH_SHORT).show();
                    materialCardView.setVisibility(View.GONE);
                    materialCardView2.setVisibility(View.VISIBLE);
                } else {
                    Toast.makeText(Home.this, "Check-in failed, please try again", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void CheckOut() {

        Calendar calendar = Calendar.getInstance();
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat mdformat = new SimpleDateFormat("HH:mm:ss");
        String time = mdformat.format(calendar.getTime());
        HashMap hashMap = new HashMap();
        hashMap.put("checkOutTime", time);

        FirebaseDatabase.getInstance().getReference("Attendence").child(userID).child(currentDate)
                .updateChildren(hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Toast.makeText(Home.this, "Check-out Succesfully", Toast.LENGTH_SHORT).show();
                    materialCardView2.setVisibility(View.GONE);
                    materialCardView.setVisibility(View.VISIBLE);
                } else {
                    Toast.makeText(Home.this, "Check-in failed, please try again", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

}
