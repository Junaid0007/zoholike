package com.example.zoholike;

public class Employee {
    private String name,email,number,password,imageUrl;
    public Employee() {

    }

    public Employee(String name, String email, String number,String imageUrl) {
        this.name = name;
        this.email = email;
        this.number = number;
        this.imageUrl=imageUrl;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

}
