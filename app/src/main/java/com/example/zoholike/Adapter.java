package com.example.zoholike;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;


public class Adapter extends RecyclerView.Adapter<Adapter.MyViewHolder> {

    private ArrayList<Attendence> attendenceArrayList;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView date,checkIn,checkOut;

        public MyViewHolder(View view) {
            super(view);
            date = (TextView) view.findViewById(R.id.date);
            checkIn = (TextView) view.findViewById(R.id.check_in_time);
            checkOut = (TextView) view.findViewById(R.id.check_out_time);
        }
    }


    public Adapter(Context context,ArrayList<Attendence> attendenceArrayList) {
        this.attendenceArrayList = attendenceArrayList;
        this.context = context;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.attenence_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        Attendence attendence = attendenceArrayList.get(position);
        holder.date.setText(attendence.getDate());
        holder.checkIn.setText(attendence.getCheckInTime());
        holder.checkOut.setText(attendence.getCheckOutTime());
    }

    @Override
    public int getItemCount() {
        return attendenceArrayList.size();
    }
}