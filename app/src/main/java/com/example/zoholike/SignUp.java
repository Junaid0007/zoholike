package com.example.zoholike;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.HashMap;

public class SignUp extends AppCompatActivity {

    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    FirebaseDatabase reff;
    Employee employee;
    private FirebaseAuth mAuth;
    private static final int PICK_IMAGE = 100;
    Uri imageUri;
    ImageView userImage;
    boolean isImageAdded=false;
    DatabaseReference databaseReference;
    StorageReference storageReference;
    ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        mAuth = FirebaseAuth.getInstance();

        final TextView topText=findViewById(R.id.top_text);
        final EditText personName = findViewById(R.id.person_name);
        final EditText email = findViewById(R.id.email);
        final EditText number = findViewById(R.id.number);
        final EditText password = findViewById(R.id.password);
        final EditText confirmPassword = findViewById(R.id.confirm_password);
        final Button signUp = findViewById(R.id.sign_up);
        employee = new Employee();
        userImage = findViewById(R.id.user_image);

        final String activity = getIntent().getStringExtra("ACTIVITY");

        progressDialog = new ProgressDialog(SignUp.this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage("Loading...please wait");
        progressDialog.setCancelable(true);

        if(activity.matches("2")){
            userImage.setVisibility(View.VISIBLE);
            databaseReference=FirebaseDatabase.getInstance().getReference("Employee")
                    .child(FirebaseAuth.getInstance().getCurrentUser().getUid());
            storageReference= FirebaseStorage.getInstance().getReference().child("UserImages");
            topText.setText("Update your profile");
            signUp.setText("Update");
        }
        else userImage.setVisibility(View.GONE);


        userImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                startActivityForResult(gallery, PICK_IMAGE);
            }
        });

        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String nameT = String.valueOf(personName.getText());
                final String emailT = String.valueOf(email.getText());
                final String numberT = String.valueOf(number.getText());
                String passwordT = String.valueOf(password.getText());
                String passwordConfirmT = String.valueOf(confirmPassword.getText());


                if (TextUtils.isEmpty(nameT) || TextUtils.isEmpty(emailT) || TextUtils.isEmpty(numberT) || TextUtils.isEmpty(passwordT) || TextUtils.isEmpty(passwordConfirmT)) {
                    Toast.makeText(SignUp.this, "All fields must be filled", Toast.LENGTH_SHORT).show();
                } else {
                    if (emailT.trim().matches(emailPattern) ) {
                        if(passwordT.matches(passwordConfirmT)) {
                            if (activity.matches("1")) {
                                mAuth.createUserWithEmailAndPassword(emailT,passwordT).addOnCompleteListener(SignUp.this,new OnCompleteListener<AuthResult>() {
                                    @Override
                                    public void onComplete(@NonNull Task<AuthResult> task) {
                                        if(task.isSuccessful()) {
                                            employee=new Employee(nameT,emailT,numberT,"");
                                            reff.getInstance().getReference("Employee")
                                                    .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                                                    .setValue(employee).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    if(task.isSuccessful()) {
                                                        Toast.makeText(SignUp.this, "Registration Succesfull", Toast.LENGTH_SHORT).show();
                                                    }
                                                    else{
                                                        Toast.makeText(SignUp.this, "Registration failed, please try again", Toast.LENGTH_SHORT).show();
                                                    }
                                                }
                                            });
                                        }
                                        else{
                                            FirebaseAuthException e = (FirebaseAuthException )task.getException();
                                            Toast.makeText(SignUp.this, "Failed Registration: "+e.getMessage(), Toast.LENGTH_LONG).show();
                                        }
                                    }
                                });
                            }
                            else {
                                HashMap hashMap = new HashMap();
                                hashMap.put("name", nameT);
                                hashMap.put("email", emailT);
                                hashMap.put("number", numberT);
                                hashMap.put("password", passwordT);


                                FirebaseDatabase.getInstance().getReference("Employee")
                                        .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                                        .updateChildren(hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()) {
                                            Toast.makeText(SignUp.this, "Updated Succesfully", Toast.LENGTH_SHORT).show();
                                        } else {
                                            Toast.makeText(SignUp.this, "Update failed, please try again", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });
                                }
                        }
                        else {
                            Toast.makeText(SignUp.this, "Passwords not match", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(SignUp.this, "Invalid Email", Toast.LENGTH_SHORT).show();
                    }
                }
                return;
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == PICK_IMAGE){
            imageUri = data.getData();
            isImageAdded=true;
            if(isImageAdded!=false) {

                userImage.setImageURI(imageUri);
                progressDialog.show();
                final String key=databaseReference.push().getKey();

                storageReference.child(key+".jpg").putFile(imageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        storageReference.child(key+".jpg").getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {

                                HashMap hashMap=new HashMap();
                                hashMap.put("imageUrl",uri.toString());
                                FirebaseDatabase.getInstance().getReference("Employee")
                                        .child(FirebaseAuth.getInstance().getCurrentUser().getUid()).updateChildren(hashMap).addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        progressDialog.dismiss();
                                        Toast.makeText(SignUp.this, "Added successfully", Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }
                        });
                    }
                });
            }
            else {
                progressDialog.dismiss();
                Toast.makeText(this, "failed", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
